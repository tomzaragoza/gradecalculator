﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradeCalculator
{
    class CourseListViewModel
    {
        /// <summary>
        /// To be filled and used with AllCourses
        /// </summary>
        public List<Course> AllCoursesList { get; set; }

        /// <summary>
        /// Constructor to create an accessible list of all the courses. Will be used to bind to the CourseGridView.
        /// </summary>
        /// <param name="CourseListWithCategory"></param>
        /// <param name="CourseList"></param>
        /// <param name="CourseCategoriesDict"></param>
        public CourseListViewModel(List<string> CourseListWithCategory, List<string> CourseList, Dictionary<string, string> CourseCategoriesDict)
        {
            var items = new List<Course>();

            foreach (string course in CourseList)
            {
                items.Add(
                    new Course {
                        CourseName = course,
                        ImageSource = SelectImageSource(course, CourseCategoriesDict)
                });
            }

            AllCoursesList = items.ToList();

        }

        /// <summary>
        /// Select the appropriate image path for the course's category.
        /// </summary>
        /// <param name="CourseName"></param>
        /// <param name="CategoriesDict"></param>
        /// <returns></returns>
        private string SelectImageSource(string CourseName, Dictionary<string, string> CategoriesDict)
        {
            if (CategoriesDict[CourseName] == "Humanities and Liberal Arts")
                return "Assets/Books.png";
            if (CategoriesDict[CourseName] == "Social Sciences")
                return "Assets/People.png";
            if (CategoriesDict[CourseName] == "Business and Commerce")
                return "Assets/Graph1.png";
            if (CategoriesDict[CourseName] == "Life and Health Sciences")
                return "Assets/DNA.png";
            if (CategoriesDict[CourseName] == "Physical and Chemical Sciences")
                return "Assets/Nodes.png";
            if (CategoriesDict[CourseName] == "Engineering")
                return "Assets/Engineering.png";
            if (CategoriesDict[CourseName] == "Computer Science")
                return "Assets/lambda.png";
            if (CategoriesDict[CourseName] == "Mathematics")
                return "Assets/Formula Support.png";
            if (CategoriesDict[CourseName] == "Other")
                return "Assets/Degree.png";
            return "Assets/Degree.png";

        }
    }

    /// <summary>
    /// The object Course with a name and image path for the corresponding symbol.
    /// </summary>
    public class Course
    {
        public string CourseName { get; set; }
        public string ImageSource { get; set; }
    }
}
