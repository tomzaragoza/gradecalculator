﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Popups;
using Windows.Storage;
using System.Reflection;

namespace GradeCalculator
{
    /// <summary>
    /// The main page where the grade calculation takes place.
    /// </summary>
    public sealed partial class MainPage : GradeCalculator.Common.LayoutAwarePage
    {
        public Dictionary<String, List<double>> GradeWeightDict = new Dictionary<String, List<double>>();
        public double CurrentGrade = new Double();
        public double CurrentWeight = new Double();
        public string SelectedItemGlobalName; // Used for checking selected item if it matches this name

        public List<string> CourseMaterialList = new List<string>();

        public string CourseName;
        public ApplicationDataContainer CourseSet = ApplicationData.Current.LocalSettings; // Stores another container for the coursework
        public ApplicationDataContainer CourseMaterial = ApplicationData.Current.LocalSettings; // Stores the course material for the specified value ie the course.
        

        public MainPage()
        {
            this.InitializeComponent();
            CourseName = gradeCalculatorTitle.Text;
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {

        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

            base.OnNavigatedTo(e);
            CourseName = e.Parameter.ToString();
            gradeCalculatorTitle.Text = CourseName;

            editGradeItemStackPanel.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            confirmDeleteStackPanel.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

            // Create the container to work with right away when the page is loaded.
            CourseSet.CreateContainer(CourseName, ApplicationDataCreateDisposition.Always);

            // Create a value in courseMaterialList for the list
            // of courses appearing in the gradestack in order.
            CreateCourseMaterialList(CourseName);

            try
            {
                AddToGradeWeightDict();
                CalculateAndDisplayGradeWeight();

                snappedGradeDisplayStackPanel.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                snappedWeightDisplayStackPanel.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            }
            catch (NullReferenceException)
            {
                // leave blank for now until I figure out what to do
                // Maybe a prompt to tell user to add a course item??
            }

            PopulateItemGridView();
        }

        /// <summary>
        /// Bind the course materials to the itemGridView to display.
        /// </summary>
        private void PopulateItemGridView()
        {
            var viewModel = new CourseMaterialViewModel(CourseMaterialList, GradeWeightDict);
            this.DataContext = viewModel;
            itemGridView.SelectionMode = ListViewSelectionMode.Single;

            // Clear first selected item since it is selected after binding.
            if (itemGridView.SelectedItems.Count != 0)
            {
                itemGridView.SelectedIndex = -1;
                itemGridView.UpdateLayout();
            }
        }

        /// <summary>
        /// Create the CourseMaterialList by splitting the string value stored in CourseMaterials.Values[coursename]
        /// </summary>
        /// <param name="CurrentCourseName"></param>
        private void CreateCourseMaterialList(string CurrentCourseName)
        {
            ListOperations instance = new ListOperations();

            if (CourseMaterial.Values.ContainsKey(CurrentCourseName))
            {
                CourseMaterialList = instance.SplitString(CourseMaterial.Values[CurrentCourseName].ToString());
            }
            else
            {
                CourseMaterial.Values[CourseName] = "";
            }
        }

        /// <summary>
        /// Boolean check for if the string can be converted into a double.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private Boolean IsDouble(String s)
        {
            double i = 0;
            bool result = double.TryParse(s, out i);
            return result;
        }

        /// <summary>
        /// Add all the course material from CourseMaterialList to the GradeWeightDict.
        /// </summary>
        private void AddToGradeWeightDict()
        {
            foreach (string id in CourseMaterialList)
            {
                ApplicationDataCompositeValue items = 
                    (ApplicationDataCompositeValue)CourseSet.Containers[CourseName].Values[id];

                // Add to the Grade and Weight dictionary so that current grade and weight
                // of total course can be added right away.
                AddGradeAndWeight(id, Convert.ToDouble(items["grade"]), Convert.ToDouble(items["weight"]));
            }
        }
        
        /// <summary>
        /// Add the course item with its corresponding grade and weight to the GradeWeightDict.
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="grade"></param>
        /// <param name="weight"></param>
        private void AddGradeAndWeight(String ID, double grade, double weight)
        {
                List<double> gradeWeightList = new List<double>();
                gradeWeightList.Add((double) grade);
                gradeWeightList.Add((double) weight);

                GradeWeightDict.Add(ID, gradeWeightList);
        }

        /// <summary>
        /// Calculates the current grade and weight of the class.
        /// </summary>
        private void CalculateCurrentGradeAndWeight()
        {
            // Clear currentWeight and currentGrade to reset for new calculation
            CurrentWeight = 0;
            CurrentGrade = 0;

            foreach (KeyValuePair<string, List<double>> entry in GradeWeightDict)
            {
                CurrentGrade += (((double)entry.Value[0] / 100)) * entry.Value[1];
                CurrentWeight += (double)entry.Value[1];
            }

            CurrentGrade = Math.Round((CurrentGrade / CurrentWeight) * (double) 100.00, 2);
            CurrentWeight = Math.Round(CurrentWeight, 2);
        }


        /// <summary>
        /// Calculate the current grade and weight of the course and display
        /// it to the provided display.
        /// </summary>
        private void CalculateAndDisplayGradeWeight()
        {
            CalculateCurrentGradeAndWeight();
            if (CurrentGrade.ToString() == "0")
            {
                txtGradeDisplay.Text = "0%";
            }
            else if (CurrentWeight.ToString() == "0")
            {
                txtWeightDisplay.Text = "0%";
            }
            else
            {
                txtGradeDisplay.Text = CurrentGrade.ToString() + "%";
                txtWeightDisplay.Text = CurrentWeight.ToString() + "%";

                snappedGradeDisplayText.Text = CurrentGrade.ToString() + "%";
                snappedWeightDisplayText.Text = CurrentWeight.ToString() + "%";
            }
        }

        /// <summary>
        /// Tapped Item event when the user selects or "taps" an item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TappedItem(object sender, TappedRoutedEventArgs e)
        {
            if (itemGridView.SelectedItems.Count != 0)
            {
                addGradeItemStackPanel.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                editGradeItemStackPanel.Visibility = Windows.UI.Xaml.Visibility.Visible;

                // populate the stackpanel with the information needed to edit
                foreach (CourseItem selected in itemGridView.SelectedItems)
                {

                    txtboxEditAssignmentTestID.Text = selected.CourseItemName;
                    SelectedItemGlobalName = selected.CourseItemName;
                    txtboxEditGrade.Text = selected.Grade.ToString();
                    txtboxEditWeight.Text = selected.Weight.ToString();

                    txtEdit.Foreground = new SolidColorBrush(selected.BackgroundColor);
                }
            }
        }

        /// <summary>
        /// Selected Changed event when user selects another item in the itemGridView
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectionChangedItem(object sender, SelectionChangedEventArgs e)
        {
            editGradeItemStackPanel.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            confirmDeleteStackPanel.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            addGradeItemStackPanel.Visibility = Windows.UI.Xaml.Visibility.Visible;

            SelectedItemGlobalName = "";

            // Clear textboxes
            txtboxEditAssignmentTestID.Text = "";
            txtboxEditGrade.Text = "";
            txtboxEditWeight.Text = "";
        }

        /// <summary>
        /// Right Tapped event when an item in the itemGridView is right tapped or right clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RightTappedItem(object sender, RightTappedRoutedEventArgs e)
        {
            editGradeItemStackPanel.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            addGradeItemStackPanel.Visibility = Windows.UI.Xaml.Visibility.Visible;

            // Clear textboxes
            txtboxEditAssignmentTestID.Text = "";
            txtboxEditGrade.Text = "";
            txtboxEditWeight.Text = "";
        }
        
        /// <summary>
        /// Click event for button AddToGradeView
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnAddToGradeView_Click(object sender, RoutedEventArgs e)
        {
            // Boolean checks for the values in the input boxes.
            Boolean inputCheck = IsDouble(txtboxGrade.Text.Trim()) & IsDouble(txtboxWeight.Text.Trim()) &
                    !(txtboxAssignmentTestID.Text == null);
            Boolean containsID = GradeWeightDict.ContainsKey(txtboxAssignmentTestID.Text.Trim());

            if (inputCheck & !containsID)
            {

                GridViewItem gradeItem = new GridViewItem();
                ApplicationDataCompositeValue courseItems = new ApplicationDataCompositeValue();

                string assignmentTestIDInput = txtboxAssignmentTestID.Text.Trim();
                string materialNameSemiColon = assignmentTestIDInput + ";";
                double gradeInput = Convert.ToDouble(txtboxGrade.Text.Trim());
                double weightInput = Convert.ToDouble(txtboxWeight.Text.Trim());

                courseItems["grade"] = gradeInput;
                courseItems["weight"] = weightInput;

                CourseSet.CreateContainer(CourseName, ApplicationDataCreateDisposition.Always);
                // Assign the assignment/test information with corresponding grade and weight as a composite value
                // into the container of the course that is passed to this page.
                CourseSet.Containers[CourseName].Values[assignmentTestIDInput] = courseItems;

                if (CourseMaterial.Values[CourseName] == null)
                {
                    CourseMaterial.Values[CourseName] = assignmentTestIDInput + ";";
                }
                else if (!(CourseMaterial.Values[CourseName] == null) &&
                                !CourseMaterial.Values[CourseName].ToString().Contains(assignmentTestIDInput))
                {
                    string courseNameAdd = string.Concat(CourseMaterial.Values[CourseName].ToString(), materialNameSemiColon);
                    CourseMaterial.Values[CourseName] = courseNameAdd;
                }

                // Add the grade and weight for the respective assignment/test to dictionary
                AddGradeAndWeight(assignmentTestIDInput, gradeInput, weightInput);

                // Clear all the textboxes.
                txtboxAssignmentTestID.Text = "";
                txtboxGrade.Text = "";
                txtboxWeight.Text = "";

                // Calculate or recalculate the grade and weight of the total course
                CalculateAndDisplayGradeWeight();

                // Create a value in courseMaterialList for the list
                // of courses appearing in the gradestack in order.
                CreateCourseMaterialList(CourseName);

                // Remake the viewModel to refresh the itemGridView.
                PopulateItemGridView();

            }
            else if (!inputCheck & !containsID)
            {
                var msgDialog = new MessageDialog("You either entered invalid values for grade and/or weight or are missing input. Please enter numbers!", "Watch out!");
                await msgDialog.ShowAsync();
            }
            else if (inputCheck & containsID)
            {
                var msgDialog = new MessageDialog("Either the assignment/test ID is missing or already exists. Please enter a new ID.", "Watch out!");
                await msgDialog.ShowAsync();
            }
        }

        /// <summary>
        /// Click event for saving edits of a gradeview item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnSaveEditGradeView_Click(object sender, RoutedEventArgs e)
        {
            Boolean inputCheck = IsDouble(txtboxEditGrade.Text.Trim()) & IsDouble(txtboxEditWeight.Text.Trim()) &
                    !(txtboxAssignmentTestID.Text == null);
            Boolean containsID = GradeWeightDict.ContainsKey(txtboxEditAssignmentTestID.Text.Trim());

            string saveName = txtboxEditAssignmentTestID.Text.Trim();

            if (inputCheck & !containsID & !(saveName == SelectedItemGlobalName))
            {
                ApplicationDataCompositeValue courseItems = new ApplicationDataCompositeValue();

                double saveGrade = Convert.ToDouble(txtboxEditGrade.Text.Trim());
                double saveWeight = Convert.ToDouble(txtboxEditWeight.Text.Trim());

                courseItems["grade"] = saveGrade;
                courseItems["weight"] = saveWeight;

                CourseSet.CreateContainer(CourseName, ApplicationDataCreateDisposition.Always);

                // Assign the assignment/test information with corresponding grade and weight as a composite value
                // into the container of the course that is passed to this page.
                CourseSet.Containers[CourseName].Values[saveName] = courseItems;

                string CourseNameReplace = CourseMaterial.Values[CourseName].ToString();
                CourseMaterial.Values[CourseName] = CourseNameReplace.Replace(SelectedItemGlobalName, saveName);

                // Remove the old name from the dictionary and replace it, adding in the new values for grade and weight
                GradeWeightDict.Remove(SelectedItemGlobalName);
                AddGradeAndWeight(saveName, saveGrade, saveWeight);

                txtboxEditAssignmentTestID.Text = "";
                txtboxEditGrade.Text = "";
                txtboxEditWeight.Text = "";

                // Calculate / recalculate the grade and weight of the whole course.
                CalculateAndDisplayGradeWeight();

                // Collapse / make visible the appropriate stackpanels
                editGradeItemStackPanel.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                addGradeItemStackPanel.Visibility = Windows.UI.Xaml.Visibility.Visible;

                // Create a value in courseMaterialList 
                CreateCourseMaterialList(CourseName);

                SelectedItemGlobalName = "";
                // Remake the viewModel to refresh the itemGridView.
                PopulateItemGridView();
                

            }
            else if (inputCheck & containsID & (saveName == SelectedItemGlobalName))
            {
                ApplicationDataCompositeValue courseItems = new ApplicationDataCompositeValue();

                double saveGrade = Convert.ToDouble(txtboxEditGrade.Text.Trim());
                double saveWeight = Convert.ToDouble(txtboxEditWeight.Text.Trim());

                courseItems["grade"] = saveGrade;
                courseItems["weight"] = saveWeight;

                CourseSet.CreateContainer(CourseName, ApplicationDataCreateDisposition.Always);
                // Assign the assignment/test information with corresponding grade and weight as a composite value
                // into the container of the course that is passed to this page.
                CourseSet.Containers[CourseName].Values[SelectedItemGlobalName] = courseItems; // can also used saveName too

                // Alter the values in the gradeWeightDict
                GradeWeightDict[saveName][0] = saveGrade;
                GradeWeightDict[saveName][1] = saveWeight;

                txtboxEditAssignmentTestID.Text = "";
                txtboxEditGrade.Text = "";
                txtboxEditWeight.Text = "";

                CalculateAndDisplayGradeWeight();

                editGradeItemStackPanel.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                addGradeItemStackPanel.Visibility = Windows.UI.Xaml.Visibility.Visible;

                // Create a value in courseMaterialList for the list
                // of courses appearing in the gradestack in order.
                CreateCourseMaterialList(CourseName);

                // Remake the viewModel to refresh the itemGridView.
                PopulateItemGridView();

            }
            else if (!inputCheck & !containsID)
            {
                var msgDialog = new MessageDialog("You either entered invalid values or are missing input. Please fix your inputs!", "Watch out!");
                await msgDialog.ShowAsync();
            }
            else if (inputCheck & containsID)
            {
                var msgDialog = new MessageDialog("Either the course material ID is missing or already exists. Please enter a new ID.", "Watch out!");
                await msgDialog.ShowAsync();
            }
            
        }

        /// <summary>
        /// Click event for confirming the deletion of a course item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConfirmDeleteCourseItem_Click(object sender, RoutedEventArgs e)
        {
            foreach (CourseItem selected in itemGridView.SelectedItems)
            {

                // Replace it in the courseMaterial string
                string toRemove = selected.CourseItemName;
                string currentItems = CourseMaterial.Values[CourseName].ToString();

                // Replace that course material that got removed in the storage.
                CourseMaterial.Values[CourseName] = currentItems.Replace(toRemove + ";", "");

                // Remove it from the gradeWeightDict so that it won't be calculated again.
                GradeWeightDict.Remove(toRemove);

            }

            // Recalculate the grade and weight
            CalculateAndDisplayGradeWeight();

            // Recreate the courseMaterialList since an item got deleted.
            CreateCourseMaterialList(CourseName);

            // Remake the viewModel to refresh the itemGridView.
            PopulateItemGridView();
        }

        /// <summary>
        /// Click event for cancelling the deletion of a course item. Closes the prompt.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancelDeleteCourseItem_Click(object sender, RoutedEventArgs e)
        {
            confirmDeleteStackPanel.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        /// <summary>
        /// Click event for displying the prompt for deleting the selected course item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeleteSelectedItem_Click(object sender, RoutedEventArgs e)
        {
            confirmDeleteStackPanel.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }
    }
    


}
