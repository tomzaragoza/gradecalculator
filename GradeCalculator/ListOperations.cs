﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradeCalculator
{
    class ListOperations
    {
        /// <summary>
        /// Splits the given string at the delimiter ';'.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public List<string> SplitString(string s)
        {
            char[] delimiter = { ';' };
            string[] courses = s.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);

            return new List<string>(courses);
        }
        /// <summary>
        /// Split the given string at delimiters ( and ). This is primarily used in
        /// the separation of course name and its category
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public List<string> SplitStringAtBrackets(string s)
        {
            char[] delimiter = { '(', ')' };
            string[] courses = s.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
            return new List<string>(courses);
        }

        /// <summary>
        /// Convert a given list to a string. Used mostly for debugging
        /// </summary>
        /// <param name="stringList"></param>
        /// <returns></returns>
        public string ListToString(List<string> stringList)
        {
            string allCourses = "";

            foreach (string id in stringList)
            {
                allCourses += id + "~";
            }

            return allCourses;
        }
    }
}
