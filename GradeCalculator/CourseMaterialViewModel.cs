﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;

namespace GradeCalculator
{
    class CourseMaterialViewModel
    {

        public List<CourseItemCategory> ItemsToCategorize { get; set; }

        /// <summary>
        /// The constructor for CourseMaterialViewModel
        /// </summary>
        /// <param name="CourseMaterialList"></param>
        /// <param name="GradeWeightDict"></param>
        public CourseMaterialViewModel(List<string> CourseMaterialList, Dictionary<string, List<double>> GradeWeightDict) 
        {
            var items = new List<CourseItem>();

            List<string> NewCourseMaterialList = SortByWeight(CourseMaterialList, GradeWeightDict);

            foreach (string id in NewCourseMaterialList)
            {
                items.Add(
                    new CourseItem { 
                        CourseItemName = id, 
                        Grade = GradeWeightDict[id][0], 
                        Weight = GradeWeightDict[id][1],
                        Effect = GetGradeEffect(GradeWeightDict[id][1]),
                        BackgroundColor = SelectColour(GradeWeightDict[id][0], GradeWeightDict[id][1])
                    });
            }

            var itemsByGradeEffect = items.GroupBy(x => x.Effect)
                .Select(x => new CourseItemCategory { ItemEffect = x.Key, Items = x.ToList() });

            ItemsToCategorize = itemsByGradeEffect.ToList();

        }

        private string GetGradeEffect(double weight)
        {
            if (weight <= 10)
                return "Low Effect";
            if (weight  > 10 && weight < 20)
                return "Medium Effect";
            if (weight >= 20)
                return "High Effect";
            return "Low";
        }

        private Color SelectColour(double grade, double weight)
        {
            if (grade < 50)
            {
                return Colors.LightGray;
            }
            else
            {
                if (weight <= 10)
                    return Colors.DodgerBlue;
                if (weight > 10 && weight < 20)
                    return Colors.LimeGreen;
                if (weight >= 20)
                    return Colors.OrangeRed;
                return Colors.HotPink;
            }
        }

        private List<string> SortByWeight(List<string> ToSort, Dictionary<string, List<double>> gradeweightdict)
        {
            List<string> Sorted = new List<string>();

            foreach (string id in ToSort)
            {
                if (gradeweightdict[id][1] <= 10)
                {
                    Sorted.Add(id);
                }
            }

            foreach (string id in ToSort)
            {
                if (gradeweightdict[id][1] > 10 && gradeweightdict[id][1] < 20)
                {
                    Sorted.Add(id);
                }
            }

            foreach (string id in ToSort)
            {
                if (gradeweightdict[id][1] >= 20)
                {
                    Sorted.Add(id);
                }
            }
            
            return Sorted;
        }

    }

    public class CourseItem
    {
        public string CourseItemName { get; set; }
        public double Grade { get; set; }
        public double Weight { get; set; }
        public string Effect { get; set; }
        public Color BackgroundColor { get; set; }
    }

    public class CourseItemCategory
    {
        public string ItemEffect { get; set; }
        public List<CourseItem> Items { get; set; }
    }
}
