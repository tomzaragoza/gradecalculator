﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Storage;
using Windows.UI;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Popups;


// The Items Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234233

namespace GradeCalculator
{
    /// <summary>
    /// A page that displays a collection of item previews.  In the Split Application this page
    /// is used to display and select one of the available groups.
    /// </summary>
    public sealed partial class CourseSelectPage : GradeCalculator.Common.LayoutAwarePage
    {
        public ApplicationDataContainer AllCourses; // the "key" is "courses"
        public List<string> CourseListWithCategory;
        public List<string> CourseList = new List<string>();
        public Boolean NavigatedToNewPage;

        private Dictionary<string, string> CourseCategoriesDict = new Dictionary<string, string>();
        private static Uri baseUri = new Uri("ms-appx:///Assets/");

        private static List<string> CourseCategoriesList = new List<string> 
                        {"Art and Design",
                         "Humanities and Liberal Arts",
                         "Social Sciences",
                         "Business and Commerce",
                         "Life and Health Sciences",
                         "Physical and Chemical Sciences",
                         "Engineering",
                         "Computer Science",
                         "Mathematics",
                         "Other",
                         };

        /// <summary>
        /// Main constructor for CourseSelectPage.
        /// </summary>
        public CourseSelectPage()
        {
            this.InitializeComponent();

            AllCourses = ApplicationData.Current.LocalSettings;

            SelectField.ItemsSource = CourseCategoriesList;

            // allCourses, a string of courses separated by ";", 
            // if it does not have the "courses" key, open a prompt so user can add one
            if (!AllCourses.Values.ContainsKey("courses"))
            {
                if (!AddCoursePrompt.IsOpen)
                {
                    AddCoursePrompt.IsOpen = true;
                }
            } else if (AllCourses.Values.ContainsKey("courses") && AllCourses.Values["courses"].ToString() == "")
            {
                if (!AddCoursePrompt.IsOpen)
                {
                    AddCoursePrompt.IsOpen = true;
                }
            }
            else
            {
                CourseListWithCategory = GetCourses();
                AddToCourseCategoryDict();
                AddToCourseList();
            }

            PopulateCourseSelectGridView();


            rightPanel.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }


        /// <summary>
        /// Set the selected item in the CourseSelectGridView to null when the 
        /// bottom app bar is closed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BottomAppBar_Closed(object sender, object e)
        {
            CourseSelectGridView.SelectedItem = null;
        }

        /// <summary>
        /// Add all courses with their corresponding categories.
        /// </summary>
        private void AddToCourseCategoryDict()
        {
            foreach (string course in CourseListWithCategory)
            {
                ListOperations listOp = new ListOperations();
                List<string> CourseAndCategory = listOp.SplitStringAtBrackets(course);
                if (!(CourseCategoriesDict.ContainsKey(CourseAndCategory[0])))
                {
                    CourseCategoriesDict.Add(CourseAndCategory[0], CourseAndCategory[1]);
                }
            }
        }

        /// <summary>
        /// Add all course names to the CourseList.
        /// </summary>
        private void AddToCourseList()
        {
            CourseList = new List<string>();
            foreach (string course in CourseListWithCategory)
            {
                ListOperations listOp = new ListOperations();
                List<string> CourseAndCategory = listOp.SplitStringAtBrackets(course);
                // Only add the course name
                CourseList.Add(CourseAndCategory[0]);
            }
        }
        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
            // TODO: Assign a bindable collection of items to this.DefaultViewModel["Items"]
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
            
        }

        /// <summary>
        /// Bind the Courses to the CourseSelectGridView
        /// </summary>
        private void PopulateCourseSelectGridView()
        {
            var _Courses = new CourseListViewModel(CourseListWithCategory, CourseList, CourseCategoriesDict);
            this.DataContext = _Courses;

            CourseSelectGridView.SelectionMode = ListViewSelectionMode.Single;

            // Clear first selected item since it is selected after binding.
            if (CourseSelectGridView.SelectedItems.Count != 0)
            {
                CourseSelectGridView.SelectedIndex = -1;
                CourseSelectGridView.UpdateLayout();
            }

            // Opens automatically. Fix that!
            MainBottomAppBar.IsOpen = false;
        }

        
        /// <summary>
        /// Close the popup when the ClosePopup button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClosePopupClicked(object sender, RoutedEventArgs e)
        {
            ClosePopup();
        }

        /// <summary>
        /// Adds the course to the course list. Takes the string input by the user and adds 
        /// it to allCourses under the key "courses". Also creates a ListViewItem for the course to be
        /// added to the courseListView.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnAddCourse_Click(object sender, RoutedEventArgs e)
        {
            // Boolean checks for input. Everything must work before adding the course.
            Boolean IsBlank = txtboxCourseNameAdd.Text == null;
            Boolean FieldIsBlank = SelectField.SelectedItem == null;
            Boolean IsInCourseList = CourseList.Contains(txtboxCourseNameAdd.Text.Trim());
            Boolean HasOnlyWhiteSpace = txtboxCourseNameAdd.Text.Length > 0 && txtboxCourseNameAdd.Text.Trim().Length == 0;

            if (!IsBlank && !FieldIsBlank && !IsInCourseList & !HasOnlyWhiteSpace)
            {
                // This is for the button inside the popup.
                string CourseNameSemiColon = txtboxCourseNameAdd.Text +
                                        "(" + SelectField.SelectedItem.ToString() + ");";
                string CourseNameListView = txtboxCourseNameAdd.Text.Trim();

                SelectField.SelectedItem = null;
                if (AllCourses.Values.ContainsKey("courses"))
                {
                    // Add new course name to start of the string.
                    AllCourses.Values["courses"] = string.Concat(CourseNameSemiColon,
                                                        AllCourses.Values["courses"].ToString());
                }
                else
                {
                    AllCourses.Values["courses"] = CourseNameSemiColon;
                }

                // Update dat.
                this.CourseSelectGridView.UpdateLayout();

                // Create CourseListWithCategory (ie raw), the courseCategoryDict for when displaying image on front page and the courseList
                // for displaying the courses without the categories in brackets on the front page.
                CourseListWithCategory = GetCourses();
                AddToCourseCategoryDict();

                AddToCourseList();

                // Populate it
                PopulateCourseSelectGridView();

                txtboxCourseNameAdd.Text = "";
                ClosePopup();
            }
            else 
            {
                var msgDialog = new MessageDialog("You're either missing input for the course name, haven't selected a field or the course already exists. Please fix your inputs!", "Careful!");
                await msgDialog.ShowAsync();
            }

        }

        /// <summary>
        /// Open prompt for user to add a course to the course ListView.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddCourseToListView_Click(object sender, RoutedEventArgs e)
        {
            if (!AddCoursePrompt.IsOpen)
            {
                AddCoursePrompt.HorizontalOffset = 2;
                AddCoursePrompt.VerticalOffset = Window.Current.CoreWindow.Bounds.Bottom - MainBottomAppBar.ActualHeight - AddCoursePromptBorder.Height;

                AddCoursePrompt.IsOpen = true;
                DeleteAllCoursesPrompt.IsOpen = false;
            }
        }

        /// <summary>
        /// Opens DeleteAllCoursesPrompt to allow the user to delete all the courses.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteAllCourses_Click(object sender, RoutedEventArgs e)
        {
            if (!DeleteAllCoursesPrompt.IsOpen)
            {
                DeleteAllCoursesPrompt.HorizontalOffset = 2;
                DeleteAllCoursesPrompt.VerticalOffset = Window.Current.CoreWindow.Bounds.Bottom - MainBottomAppBar.ActualHeight - DeleteAllCoursesPromptBorder.Height;

                DeleteAllCoursesPrompt.IsOpen = true;
                AddCoursePrompt.IsOpen = false;
            }
        }

        /// <summary>
        /// Closes any of the two possible (currently) popups if they are opened.
        /// </summary>
        private void ClosePopup()
        {
            if (AddCoursePrompt.IsOpen)
            {
                AddCoursePrompt.IsOpen = false;
            }
            else if (DeleteAllCoursesPrompt.IsOpen)
            {
                DeleteAllCoursesPrompt.IsOpen = false;
            }
            else if (DeleteSelectedCoursePrompt.IsOpen)
            {
                DeleteSelectedCoursePrompt.IsOpen = false;
            }
        }

        /// <summary>
        /// Fill the course ListView. Usually called when
        /// the CourseSelectPage is loaded.
        /// </summary>
        /// <param name="courseList"></param>
        private void FillGridView(List<string> courseList)
        {
            foreach (string courseName in courseList)
            {
                GridViewItem courseItem = CreateGridViewItem(courseName);
                CourseSelectGridView.Items.Add(courseItem);
            }
        }

        /// <summary>
        /// Creates a ListViewItem to be added into the course ListView after creation.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private GridViewItem CreateGridViewItem(string name)
        {
            GridViewItem course = new GridViewItem();
            course.Content = name;
            course.HorizontalContentAlignment = HorizontalAlignment.Center;
            course.VerticalContentAlignment = VerticalAlignment.Center;
            return course;
        }

        /// <summary>
        /// Get all the courses by splitting the string at the 
        /// allCourses value at "courses". 
        /// </summary>
        /// <returns></returns>
        private List<string> GetCourses()
        {
            ListOperations listOp = new ListOperations();
            return listOp.SplitString(AllCourses.Values["courses"].ToString());
        }

        /// <summary>
        /// Deletes all the courses from the list and allCourses.Values["course"]
        /// when the user confirms/clicks the confirm delete button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConfirmDeleteAllCourses_Click(object sender, RoutedEventArgs e)
        {
            AllCourses.Values["courses"] = "";

            // This foreach loop deletes all the material for each course so readding them results in a clean slate.
            foreach (string course in CourseList)
            {
                MainPage instance = new MainPage();
                instance.CourseSet.DeleteContainer(course);
                instance.CourseMaterial.Values[course] = null; // Deleting Course? Delete the Course Material, too!

            }
            // Create courseListWithCategory (ie raw), the courseCategoryDict for when displaying image on front page and the courseList
            // for displaying the courses without the categories in brackets on the front page.
            CourseListWithCategory = GetCourses();

            AddToCourseList();
            PopulateCourseSelectGridView();

            this.CourseSelectGridView.UpdateLayout();
            ClosePopup();
        }

        /// <summary>
        /// Click event for the confirmation of deleting a selected course.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConfirmDeleteSelectedCourse_Click(object sender, RoutedEventArgs e)
        {
            foreach (Course selected in CourseSelectGridView.SelectedItems)
            {
                string CurrentAllCourses = AllCourses.Values["courses"].ToString();

                int CourseIndex = CourseList.FindIndex(item => item == selected.CourseName);

                string toDelete = CourseListWithCategory[CourseIndex] + ";";
                string modifiedAllCourses = CurrentAllCourses.Replace(toDelete, ""); // delete from old value in allCourses.Values["courses"]

                // Remove from CourseListWithCategory and courseCategoriesDict since they are now deleted
                CourseListWithCategory.RemoveAt(CourseIndex);
                CourseCategoriesDict.Remove(selected.CourseName);

                AllCourses.Values["courses"] = modifiedAllCourses; // re-add the new AllCourses with course you just deleted into local storage.

                // Make an instance of MainPage so we can access 
                // the ApplicationDataContainer courseSet and delete the course from that!
                MainPage instance = new MainPage();
                instance.CourseSet.DeleteContainer(selected.CourseName);
                instance.CourseMaterial.Values[selected.CourseName] = null; // Deleting Course? Delete the Course Material, too!

                // Create courseListWithCategory (ie raw), the courseCategoryDict for when displaying image on front page and the courseList
                // for displaying the courses without the categories in brackets on the front page.
                CourseListWithCategory = GetCourses();

                AddToCourseList();

                PopulateCourseSelectGridView();
            }

            ClosePopup();
        }

        /// <summary>
        /// Delete the selected course in the course ListView. Also deletes
        /// the course information from MainPage so there is no duplicates.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteCourseInGridView_Click(object sender, RoutedEventArgs e)
        {
            if (!DeleteSelectedCoursePrompt.IsOpen)
            {
                DeleteSelectedCoursePrompt.IsOpen = true;
            }
        }

        /// <summary>
        /// Navigate to MainPage with the corresponding course name.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NavigateToSelectedCourse_Click(object sender, RoutedEventArgs e)
        {
            foreach (Course selected in CourseSelectGridView.SelectedItems)
            {
                string NavigateTo = selected.CourseName;


                this.Frame.Navigate(typeof(MainPage), NavigateTo);
            }
        }

        /// <summary>
        /// Open SelectedCourseOptionsStackPanel when an item
        /// in the course ListView is right clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RightTappedItem(object sender, RightTappedRoutedEventArgs e)
        {
            MainBottomAppBar.IsOpen = false;
            leftPanel.Visibility = Windows.UI.Xaml.Visibility.Visible;
            rightPanel.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        /// <summary>
        /// Close / collapse the selectedCourseOptionsStackPanel
        /// when an item is deselected.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CourseSelectGridView.SelectedItem != null)
            {
                BottomAppBar.IsOpen = true;
                leftPanel.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                rightPanel.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }
            else
            {   // So that if nothing is selected, the leftPanel with Add Course and Delete all Courses buttons are displayed
                leftPanel.Visibility = Windows.UI.Xaml.Visibility.Visible;
                rightPanel.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            }
            
            foreach (Course selected in CourseSelectGridView.SelectedItems)
            {
                string NavigateTo = selected.CourseName;
            }
        }

        /// <summary>
        /// Click event for showing the DeleteSelectedCoursePrompt.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteSelectedCourseMenuButton_Click(object sender, RoutedEventArgs e)
        {
            DeleteSelectedCoursePrompt.HorizontalOffset = Window.Current.CoreWindow.Bounds.Width - DeleteSelectedCoursePrompt.Width + 6;
            DeleteSelectedCoursePrompt.VerticalOffset = Window.Current.CoreWindow.Bounds.Bottom - 2.5 * MainBottomAppBar.ActualHeight -  DeleteSelectedCoursePrompt.Height;

            DeleteSelectedCoursePrompt.IsOpen = true;
        }

        
    }
}
