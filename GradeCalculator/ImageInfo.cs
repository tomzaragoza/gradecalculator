﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradeCalculator
{
    class ImageInfo
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Uri Image { get; set; }
    }
}
